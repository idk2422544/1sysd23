#include <stdio.h>

float inches2cm(float k) {
    float p;
    p = k * 2.54;
    return p;
}

float cm2inches(float k) {
    float p;
    p = k / 2.54;
    return p;
}

int main() {
    float k;
    int n;
    printf("veuillez rentrer votre mode de convertion : 1 pour inches à centimètre ou 2 pour centimètres à inches \n");
    scanf("%d", &n);
    printf("veuillez rentrer la valeur à convertir \n ");
    scanf("%f", &k);
    if (n == 1) {
        printf("le resultat de votre conversion est : %.2f", inches2cm(k), "cm"); 
    }
    if (n == 2) {
        printf("le resultat de votre conversion est : %.2f", cm2inches(k), "inch");
    }
    
}
