#include <stdio.h>

int main() {
	
	int counter = 0;
	char *argv[] = {"to be or not to be", "Ici 42 et 54 puis 1"};
	int argc = sizeof(argv) / sizeof(char*);

	for(int i = 0; i < argc; i++) {
		char *str = argv[i];
		while(*str) {
			if(*str >= '0' && *str <= '9') {
				counter++;
				}
			str++;
		}
	}

	printf("Le nombre total de chiffres trouvés est : %d\n", counter);

	return 0;
}

